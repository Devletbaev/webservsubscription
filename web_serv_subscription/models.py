import enum

from sqlalchemy import Column, Enum, Integer, String, ForeignKey, Float
from sqlalchemy.dialects.postgresql import ARRAY, UUID
from sqlalchemy.orm import relationship

from .database import Base


class Sub(Base):
    __tablename__ = "subs"

    class SubStatus(str, enum.Enum):
        active = "active"
        disabled = "disabled"
        deleted = "deleted"

    class Resolution(str, enum.Enum):
        low = "low"
        medium = "medium"
        high = "high"
        ultra_high = "ultra_high"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(UUID)
    user_email = Column(String)
    roi = Column(String)
    title = Column(String)
    max_cloudiness = Column(Integer)
    min_intersection = Column(Integer, default=0)
    resolution = Column(Enum(Resolution))
    status = Column(Enum(SubStatus), default=SubStatus.active)
    notifications = relationship("Notification", back_populates="subscription")


class Notification(Base):
    __tablename__ = "notifications"

    class NotificationStatus(str, enum.Enum):
        sent = "sent"
        cooking = "cooking"
        canceled = "canceled"

    id = Column(Integer, primary_key=True, index=True)
    sub_id = Column(Integer, ForeignKey("subs.id"))
    status = Column(Enum(NotificationStatus), default=NotificationStatus.cooking)
    images = Column(ARRAY(String))

    subscription = relationship("Sub", back_populates="notifications")
