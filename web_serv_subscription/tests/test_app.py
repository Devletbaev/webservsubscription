import pytest
from fastapi.testclient import TestClient
from keycloak import KeycloakOpenID
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import CreateSchema
from sqlalchemy_utils import create_database, database_exists, drop_database

from web_serv_subscription.database import Base
from web_serv_subscription.dependencies import get_db
from web_serv_subscription.main import app
from web_serv_subscription.settings import config


@pytest.fixture(scope="session")
def test_app():
    datebase_url = config["testing"]["database_url"]
    engine = create_engine(datebase_url)
    if not database_exists(datebase_url):
        create_database(datebase_url)
        engine.execute(CreateSchema("subscription"))
        Base.metadata.create_all(engine)
    session = sessionmaker(autocommit=False, autoflush=False, bind=engine)

    def get_db_test():
        db = session()
        try:
            yield db
        finally:
            db.close()

    app.dependency_overrides[get_db] = get_db_test
    yield TestClient(app)
    drop_database(datebase_url)


@pytest.fixture(scope="session")
def auth_header():
    keycloak_openid = KeycloakOpenID(**config["keycloak"])
    token = keycloak_openid.token(
        config["testing"]["username"], config["testing"]["password"]
    )["access_token"]
    return {"Authorization": f"Bearer {token}"}


def test_ht(test_app):
    response = test_app.get("/ht")
    assert response.status_code == 200


def test_create_subscription(test_app, auth_header):
    response = test_app.post(
        "/subs",
        headers=auth_header,
        json={
            "title": "test",
            "roi": "POLYGON ((-9.91542078886659 40.7400908655858,-8.58004016207937 35.6797011219711,-9.14230568914768 48.1901090992409,-14.413545005413 44.8165159368311,-14.2026954327624 40.0372589567505,-15.7489256322003 36.8393737715495,-13.4647219284853 35.4688515493205,-11.988774919931 36.8745153669913,-11.9184917290474 39.4749934296822,-9.91542078886659 40.7400908655858))",
            "max_cloudiness": 80,
            "min_intersection": 10,
            "resolution": "high",
        },
    )
    assert response.status_code == 200
    assert response.json() == {
        "title": "test",
        "roi": "POLYGON ((-9.91542078886659 40.7400908655858,-8.58004016207937 35.6797011219711,-9.14230568914768 48.1901090992409,-14.413545005413 44.8165159368311,-14.2026954327624 40.0372589567505,-15.7489256322003 36.8393737715495,-13.4647219284853 35.4688515493205,-11.988774919931 36.8745153669913,-11.9184917290474 39.4749934296822,-9.91542078886659 40.7400908655858))",
        "max_cloudiness": 80,
        "min_intersection": 10.0,
        "resolution": "high",
        "status": "active",
        "user_id": "6225e1dc-7f37-4bc8-b1b9-5ddb7af3aef6",
        "user_email": "customer_niitp@dep45.niitp",
    }


def test_create_subscription_invalid(test_app, auth_header):
    response = test_app.post(
        "/subs",
        headers=auth_header,
        json={
            "title": "test",
            "roi": "BAD POLYGON",
            "max_cloudiness": 120,
            "min_intersection": -2,
            "resolution": "nonexistent_value",
        },
    )
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "roi"],
                "msg": "Roi must be geometry",
                "type": "value_error",
            },
            {
                "loc": ["body", "max_cloudiness"],
                "msg": "ensure this value is less than or equal to 100",
                "type": "value_error.number.not_le",
                "ctx": {"limit_value": 100},
            },
            {
                "loc": ["body", "min_intersection"],
                "msg": "ensure this value is greater than or equal to 0",
                "type": "value_error.number.not_ge",
                "ctx": {"limit_value": 0},
            },
            {
                "loc": ["body", "resolution"],
                "msg": "value is not a valid enumeration member; permitted: 'low', 'medium', 'high', 'ultra_high'",
                "type": "type_error.enum",
                "ctx": {"enum_values": ["low", "medium", "high", "ultra_high"]},
            },
        ]
    }


def test_get_subs(test_app, auth_header):
    response = test_app.get(
        "/subs",
        headers=auth_header,
    )
    assert response.status_code == 200
    assert response.json() == [
        {
            "title": "test",
            "roi": "POLYGON ((-9.91542078886659 40.7400908655858,-8.58004016207937 35.6797011219711,-9.14230568914768 48.1901090992409,-14.413545005413 44.8165159368311,-14.2026954327624 40.0372589567505,-15.7489256322003 36.8393737715495,-13.4647219284853 35.4688515493205,-11.988774919931 36.8745153669913,-11.9184917290474 39.4749934296822,-9.91542078886659 40.7400908655858))",
            "max_cloudiness": 80,
            "min_intersection": 10.0,
            "resolution": "high",
            "status": "active",
            "user_id": "6225e1dc-7f37-4bc8-b1b9-5ddb7af3aef6",
            "user_email": "customer_niitp@dep45.niitp",
        }
    ]


def test_disable_subs(test_app, auth_header):
    response = test_app.post("/subs/disable", headers=auth_header)
    assert response.status_code == 200


def test_get_subs_after_disable(test_app, auth_header):
    response = test_app.get(
        "/subs",
        headers=auth_header,
    )
    assert response.status_code == 200
    assert response.json() == [
        {
            "title": "test",
            "roi": "POLYGON ((-9.91542078886659 40.7400908655858,-8.58004016207937 35.6797011219711,-9.14230568914768 48.1901090992409,-14.413545005413 44.8165159368311,-14.2026954327624 40.0372589567505,-15.7489256322003 36.8393737715495,-13.4647219284853 35.4688515493205,-11.988774919931 36.8745153669913,-11.9184917290474 39.4749934296822,-9.91542078886659 40.7400908655858))",
            "max_cloudiness": 80,
            "min_intersection": 10.0,
            "resolution": "high",
            "status": "disabled",
            "user_id": "6225e1dc-7f37-4bc8-b1b9-5ddb7af3aef6",
            "user_email": "customer_niitp@dep45.niitp",
        }
    ]


def test_enabale_subs(test_app, auth_header):
    response = test_app.post("/subs/enable", headers=auth_header)
    assert response.status_code == 200


def test_delete_subs(test_app, auth_header):
    response = test_app.delete("/subs", headers=auth_header)
    assert response.status_code == 200


def test_get_subs_after_delete(test_app, auth_header):
    response = test_app.get("/subs", headers=auth_header)
    assert response.status_code == 200
    assert response.json() == []
