import sentry_sdk
import uvicorn
from fastapi import FastAPI
from sentry_asgi import SentryMiddleware

from web_serv_subscription.routers import ht, subs
from web_serv_subscription.settings import config

sentry_sdk.init(**config["sentry"])

app = FastAPI(
    title="webServSubscription",
    description="The webservice is used to monitor the emergence of new images in user-specified ROI",
    version="0.1.0",
    openapi_tags=[
        {
            "name": "subs",
            "description": "Operations with subs. For all operations, you must send **Bearer Token**.",
        }
    ],
    root_path=config["server_path"],
)
app.include_router(subs.router)
app.include_router(ht.router)
# app = SentryMiddleware(app)

# if __name__ == "__main__":
#     uvicorn.run(app, host="localhost", port=8000)
