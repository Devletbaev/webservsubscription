from typing import List
from uuid import UUID
import enum

from pydantic import BaseModel, Field, validator
from pygeos import Geometry, GEOSException, get_type_id, is_valid


class Notification(BaseModel):
    id: int
    sub_id: int
    status: int
    images: List

    class Config:
        orm_mode = True


class Resolution(str, enum.Enum):
    low = "low"
    medium = "medium"
    high = "high"
    ultra_high = "ultra_high"


class SubBase(BaseModel):
    title: str = Field(..., title="Имя подписки")
    roi: str = Field(..., title="Район интереса")
    max_cloudiness: int = Field(
        100, ge=0, le=100, title="Максимальная облачность интересующих снимков"
    )
    min_intersection: float = Field(
        0,
        ge=0,
        le=100,
        title="Минимальное покрытие района интереса для отправки уведомления",
    )
    resolution: Resolution = Field(title="Тип разрешения")

    @validator("roi")
    def roi_validate(cls, value):
        try:
            roi = Geometry(value)
        except GEOSException:
            raise ValueError("Roi must be geometry")
        if get_type_id(roi) != 3:
            raise ValueError("Roi must be Polygon")
        if not is_valid(roi):
            raise ValueError("Invalid Polygon")
        return value


class SubCreate(SubBase):
    class Config:
        schema_extra = {
            "example": {
                "roi": "POLYGON ((30.00134170264066 61.66357745611052, 29.99367416288298 61.6623552514454, 29.99447524756253 61.65491233939858, 30.00357330577974 61.65483083774211, 30.00591934285187 61.65863401980164, 30.00425995128755 61.66243673326051, 30.00134170264066 61.66357745611052))",
                "max_cloudiness": 15,
                "min_intersection": 5,
                "resolution": "low",
            }
        }


class Sub(SubBase):
    status: str
    user_id: UUID
    user_email: str

    # notifications: List[Notification]

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "roi": "POLYGON ((30.00134170264066 61.66357745611052, 29.99367416288298 61.6623552514454, 29.99447524756253 61.65491233939858, 30.00357330577974 61.65483083774211, 30.00591934285187 61.65863401980164, 30.00425995128755 61.66243673326051, 30.00134170264066 61.66357745611052))",
                "max_cloudiness": 15,
                "min_intersection": 0.0,
                "resolution": "low",
                "status": "active",
                "user_id": "6225e1dc-7f37-4bc8-b1b9-5ddb7af3aef6",
                "user_email": "customer_niitp@dep45.niitp",
            }
        }
