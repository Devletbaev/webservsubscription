from fastapi_utils.inferring_router import APIRouter

router = APIRouter()


@router.get("/ht", tags=["healthcheck"])
def ht():
    return {"detail": "ok"}
