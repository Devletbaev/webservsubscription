from typing import List, Optional

from fastapi import Depends, HTTPException, Query
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from sqlalchemy.orm import Session

from web_serv_subscription import crud, schemas
from web_serv_subscription.dependencies import get_db, get_user_params

router = InferringRouter()


@cbv(router)
class SubCBV:
    db: Session = Depends(get_db)
    userinfo: dict = Depends(get_user_params)

    @router.get("/subs", response_model=List[schemas.Sub], tags=["subs"])
    def get_subs(
        self,
        deleted: Optional[bool] = Query(
            False,
            description="Query that indicates whether to include deleted subscriptions in the response",
        ),
    ):
        db_sub = crud.get_subs(db=self.db, user_id=self.userinfo["id"], deleted=deleted)
        if db_sub is None:
            raise HTTPException(status_code=404, detail="You have no subscriptions")
        return db_sub

    @router.get("/subs/{sub_id}", response_model=schemas.Sub, tags=["subs"])
    def get_sub(
        self,
        sub_id: int,
        deleted: Optional[bool] = Query(
            False,
            description="Query that indicates whether to include deleted subscriptions in the response",
        ),
    ):
        db_sub = crud.get_sub(
            db=self.db, sub_id=sub_id, user_id=self.userinfo["id"], deleted=deleted
        )
        if db_sub is None:
            raise HTTPException(status_code=404, detail="Subscription not found")
        return db_sub

    @router.post("/subs", response_model=schemas.Sub, tags=["subs"])
    def create_sub(
        self,
        sub: schemas.SubCreate,
    ):
        return crud.create_sub(
            db=self.db,
            sub=sub,
            user_id=self.userinfo["id"],
            user_email=self.userinfo["email"],
        )

    @router.delete("/subs/{sub_id}", tags=["subs"])
    def delete_sub(
        self,
        sub_id: int,
    ):
        db_sub = crud.delete_sub(db=self.db, sub_id=sub_id, user_id=self.userinfo["id"])
        if db_sub is None:
            raise HTTPException(status_code=404, detail="Subscription not found")
        return {"message": "Subscription successfully deleted"}

    @router.delete("/subs", tags=["subs"])
    def delete_subs(self):
        db_sub = crud.delete_subs(db=self.db, user_id=self.userinfo["id"])
        if db_sub is None:
            raise HTTPException(status_code=404, detail="You have no subscriptions")
        return {"message": "Subscriptions successfully deleted"}

    @router.post("/subs/enable", tags=["subs"])
    def enable_subs(self):
        db_sub = crud.enable_subs(db=self.db, user_id=self.userinfo["id"])
        if db_sub is None:
            raise HTTPException(
                status_code=404, detail="Your notifications are already enabled"
            )
        return {"message": "Your notifications are enabled"}

    @router.post("/subs/disable", tags=["subs"])
    def disable_subs(self):
        db_sub = crud.disable_subs(db=self.db, user_id=self.userinfo["id"])
        if db_sub is None:
            raise HTTPException(
                status_code=404, detail="Your notifications are already disabled"
            )
        return {"message": "Your notifications are disabled"}
