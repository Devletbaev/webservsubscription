from uuid import UUID

from sqlalchemy import and_, or_
from sqlalchemy.orm import Session

from . import models, schemas


def get_sub(db: Session, sub_id: int, user_id: UUID, deleted: bool):
    if deleted:
        return (
            db.query(models.Sub)
            .filter(and_(models.Sub.id == sub_id, models.Sub.user_id == user_id))
            .first()
        )
    else:
        return (
            db.query(models.Sub)
            .filter(
                and_(
                    models.Sub.id == sub_id,
                    models.Sub.user_id == user_id,
                    or_(models.Sub.status == "active", models.Sub.status == "disabled"),
                )
            )
            .first()
        )


def get_subs(db: Session, user_id: UUID, deleted: bool):
    if deleted:
        return db.query(models.Sub).filter(models.Sub.user_id == user_id).all()
    else:
        return (
            db.query(models.Sub)
            .filter(
                and_(
                    models.Sub.user_id == user_id,
                    (
                        or_(
                            models.Sub.status == "active",
                            models.Sub.status == "disabled",
                        )
                    ),
                )
            )
            .all()
        )


def create_sub(db: Session, sub: schemas.SubCreate, user_id: UUID, user_email: str):
    db_sub = models.Sub(
        roi=sub.roi,
        max_cloudiness=sub.max_cloudiness,
        user_id=user_id,
        user_email=user_email,
        title=sub.title,
        resolution=sub.resolution,
        min_intersection=sub.min_intersection,
    )
    db_notific = models.Notification(subscription=db_sub)
    db.add(db_notific)
    db.commit()
    return db_sub


def delete_sub(db: Session, sub_id: int, user_id: UUID):
    db_sub = (
        db.query(models.Sub)
        .filter(
            and_(
                models.Sub.id == sub_id,
                models.Sub.user_id == user_id,
                models.Sub.status == "active",
            )
        )
        .first()
    )
    if db_sub:
        db_sub.status = "deleted"
        db.query(models.Notification).filter(
            and_(
                models.Notification.subscription == db_sub,
                models.Notification.status == "cooking",
            )
        ).update({"status": "canceled"})
        db.commit()
    return db_sub


def delete_subs(db: Session, user_id: UUID):
    user_subs = db.query(models.Sub).filter(
        models.Sub.user_id == user_id,
        and_(or_(models.Sub.status == "active", models.Sub.status == "disabled")),
    )
    user_subs_ids = list(map(lambda x: x.id, list(user_subs)))
    db_sub = user_subs.update({"status": "deleted"})
    db_notic = (
        db.query(models.Notification)
        .filter(models.Notification.sub_id.in_(user_subs_ids))
        .update({"status": "canceled"})
    )
    if db_sub:
        db.commit()
    return db_sub


def disable_subs(db: Session, user_id: UUID):
    user_subs = db.query(models.Sub).filter(
        and_(models.Sub.user_id == user_id, models.Sub.status == "active")
    )
    user_subs_ids = list(map(lambda x: x.id, list(user_subs)))
    db_sub = user_subs.update({"status": "disabled"})
    db_notic = (
        db.query(models.Notification)
        .filter(models.Notification.sub_id.in_(user_subs_ids))
        .update({"status": "canceled"})
    )
    if db_sub:
        db.commit()
    return db_sub


def enable_subs(db: Session, user_id: UUID):
    user_subs = db.query(models.Sub).filter(
        and_(models.Sub.user_id == user_id, models.Sub.status == "disabled")
    )
    user_subs_ids = list(map(lambda x: x.id, list(user_subs)))
    db_sub = user_subs.update({"status": "active"})
    new_notics = []
    for sub_id in user_subs_ids:
        new_notics.append(models.Notification(sub_id=sub_id))
    db.bulk_save_objects(new_notics)
    if db_sub:
        db.commit()
    return db_sub
