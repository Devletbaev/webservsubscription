from fastapi import Depends, HTTPException
from fastapi.security import HTTPBearer
from keycloak import KeycloakOpenID
from keycloak.exceptions import (KeycloakAuthenticationError,
                                 KeycloakConnectionError)

from web_serv_subscription.database import SessionLocal
from web_serv_subscription.settings import config

http_credentials = HTTPBearer()


def get_user_params(credentials: str = Depends(http_credentials)):
    keycloak_openid = KeycloakOpenID(**config["keycloak"])
    try:
        userinfo = keycloak_openid.userinfo(credentials.credentials)
    except (KeycloakAuthenticationError, KeycloakConnectionError):
        raise HTTPException(status_code=500, detail="Authentication error")
    return {
        "id": userinfo["sub"],
        "email": userinfo["email"],
    }


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
