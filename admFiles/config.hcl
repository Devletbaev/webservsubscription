template {
  contents = "{{ $keys_path := printf \"%s/%s/settings.yml\" (env \"ENVIRONMENT\") (env \"APPID\") -}}{{ key $keys_path }}"
  destination = "/etc/opt/webServSubscription/settings.yml"
  create_dest_dirs = true
  perms = 0644
}

exec {
  command = "uvicorn web_serv_subscription.main:app --host 0.0.0.0 --port 80"
  reload_signal = "SIGHUP"
}

