FROM gl-registry.ext-dc1.o.nkpor.etris/adm/images/centosbasegdal/centos_base_gdal:latest-gdal320
#
# Consul settings
ENV ENVIRONMENT=development \
    APPID=web-serv-subscription \
    HOST_IP="http://consul01.ext-dc1.o.nkpor.etris/"

ADD admFiles/*.hcl    /etc/consul/
ADD admFiles/env.sh   /tmp/
ADD admFiles/pip.conf /root/.pip/pip.conf
ADD admFiles/pypirc   /root/.pypirc

RUN set -xv ;\
    yum makecache ;\
    yum -y install gcc-c++ python3-devel postgresql-devel curl \
                   kernel-headers geos-devel glibc-devel openssl ;\
    yum clean all ;\
    rm -rf /var/cache/yum/ ;\
    source /tmp/env.sh ;\
    pip3 install fastapi_utils ;\
    pip3 install ${PACKAGE_NAME}==${PACKAGE_VERSION} ;\
#
# consul settings
    SHARE="http://nexus.ext-dc1.o.nkpor.etris:8081/repository/share/consul" ;\
    FILENAME="consul-template_0.25.0_linux_amd64.tgz" ;\
    curl --silent --output ${FILENAME} ${SHARE}/${FILENAME} ;\
    tar xvzf ${FILENAME} -C /usr/bin/ ;\
    rm -f ${FILENAME} ;\
#
    python  --version ;\
    python3 --version ;\
    pip3    --version ;\
    pip3 list
#
# Параметр приходится указывать вручную, т.к. переменная ${PACKAGE_NAME} здесь не сработает
#WORKDIR /etc/opt/orderStatusUpdater
#
ENTRYPOINT /usr/bin/consul-template -consul-addr "${HOST_IP}:8500" -config /etc/consul/config.hcl
#
# CMD ["uvicorn", "web_serv_subscription.main:app", "--host", "0.0.0.0", "--port", "80"]
